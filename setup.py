import setuptools
from sockterm.version import VERSION

setuptools.setup(
    version=VERSION
)
